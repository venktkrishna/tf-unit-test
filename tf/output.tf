output "task_definition" {
  value = module.ecs.task_definition
}
output "bucket_id" {
  value = module.s3.bucket_id
}

output "bucket_arn" {
  value = module.s3.bucket_arn
}

output "logging_target_bucket" {
  value = module.s3.logging_target_bucket
}

output "logging_target_prefix" {
  value = module.s3.logging_target_prefix
}