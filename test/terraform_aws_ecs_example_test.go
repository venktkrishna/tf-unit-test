package test

import (
	"fmt"
	"os"
	"testing"

	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"

	awsSDK "github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
)

// An example of how to test the Terraform module in examples/terraform-aws-ecs-example using Terratest.
func TestTerraformAwsEcsExample(t *testing.T) {
	t.Parallel()

    os.Setenv("AWS_ACCESS_KEY_ID", "AKIAY5OIRIASJ5MUQZ7X")
    os.Setenv("AWS_SECRET_ACCESS_KEY", "lCmwHRzUbAzeVQxVtpcyc8igjgIoUycltl69qngW")

    // Get the value of an Environment Variable
      accesskey := os.Getenv("AWS_ACCESS_KEY_ID")
      secretkey := os.Getenv("AWS_SECRET_ACCESS_KEY")

	expectedClusterName := fmt.Sprintf("terratest-aws-ecs-example-cluster-%s", random.UniqueId())
	expectedServiceName := fmt.Sprintf("terratest-aws-ecs-example-service-%s", random.UniqueId())
//     expectedName := fmt.Sprintf("terratest-aws-s3-example-%s", strings.ToLower(random.UniqueId()))

//     expectedEnvironment := "Automated Testing"
	// Pick a random AWS region to test in. This helps ensure your code works in all regions.
	awsRegion := aws.GetRandomStableRegion(t, []string{"us-east-2"}, nil)
// 	awsRegion := aws.GetRandomStableRegion(t, nil, nil)

	// Construct the terraform options with default retryable errors to handle the most common retryable errors in
	// terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: "../tf",

		// Variables to pass to our Terraform code using -var options
// 		Vars: map[string]interface{}{
// 			"cluster_name": expectedClusterName,
// 			"service_name": expectedServiceName,
// // 			"tag_bucket_name":        expectedName,
//             "tag_bucket_environment": expectedEnvironment,
//             "with_policy":            "true",
// 		},

		// Environment variables to set when running Terraform
		EnvVars: map[string]string{
			"AWS_DEFAULT_REGION": awsRegion,
			"AWS_ACCESS_KEY_ID": accesskey,
			"AWS_SECRET_ACCESS_KEY": secretkey,
		},
	})

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

	// Run `terraform output` to get the value of an output variable
	taskDefinition := terraform.Output(t, terraformOptions, "task_definition")
	bucketID := terraform.Output(t, terraformOptions, "bucket_id")

	// Look up the ECS cluster by name
	cluster := aws.GetEcsCluster(t, awsRegion, expectedClusterName)

	assert.Equal(t, int64(1), awsSDK.Int64Value(cluster.ActiveServicesCount))

	// Look up the ECS service by name
	service := aws.GetEcsService(t, awsRegion, expectedClusterName, expectedServiceName)

	assert.Equal(t, int64(0), awsSDK.Int64Value(service.DesiredCount))
	assert.Equal(t, "FARGATE", awsSDK.StringValue(service.LaunchType))

	// Look up the ECS task definition by ARN
	task := aws.GetEcsTaskDefinition(t, awsRegion, taskDefinition)

    expectedLogsTargetBucket := fmt.Sprintf("%s-logs", bucketID)
    expectedLogsTargetPrefix := "TFStateLogs/"
    loggingTargetBucket := aws.GetS3BucketLoggingTarget(t, awsRegion, bucketID)
    loggingObjectTargetPrefix := aws.GetS3BucketLoggingTargetPrefix(t, awsRegion, bucketID)

	assert.Equal(t, "256", awsSDK.StringValue(task.Cpu))
	assert.Equal(t, "512", awsSDK.StringValue(task.Memory))
	assert.Equal(t, "awsvpc", awsSDK.StringValue(task.NetworkMode))
	assert.Equal(t, expectedLogsTargetBucket, loggingTargetBucket)
    assert.Equal(t, expectedLogsTargetPrefix, loggingObjectTargetPrefix)
    }