variable "cluster_name" {
  description = "The name to set for the ECS cluster."
}

variable "service_name" {
  description = "The name to set for the ECS service."
}
variable "launch_type" {

}
variable "network_mode" {}
variable "cpu" {}
variable "memory" {}