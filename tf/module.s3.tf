module "s3" {
  source = "../modules/s3"
  tag_bucket_environment = "Test"
  tag_bucket_name = "testbucket"
  with_policy = false
}