module "ecs" {
  source = "../modules/ecs"
  cpu = 256
  launch_type = "FARGATE"
  memory = 512
  network_mode = "awsvpc"
  cluster_name = "terratest-example"
  service_name = "terratest-example"
}